Majremind
=========
A simple program which tells me which server hasn't been updated recently. Servers are stored into a Dict, and ordered by timestamp.

What is used
------------
It uses DETS, Disk Erlang Term Storage system to load the dicts I need.

How to use it
-------------
To build the escript, you will need erlang and elixir (provided by your system's packet manager or taken from github).
Then in this directory, you type `mix escript.build` and *poof*! magic!

```
USAGE: 
$ ./majremind -h | --help  
$ ./majremind -l | --list # not an ordered one 
$ ./majremind -L | --last  
$ ./majremind -t | --take-care-of "server that has been upgraded recently"
$ ./majremind -a | --add "new server"
$ ./majremind -r | --remove "server to remove"
```

You can use the functions directly from IEx, by typing:
`$ iex -S mix`


A nice way to use it would be to interface it with a shell script and `libnotify`:

```shell
#!/usr/bin/env bash
server=$(majremind -L)
notify "Server" "Lastest server to be updated is ${server}"
```
Put this script in your $PATH (I usually put it in `~/.local/bin/`) and bind a key to it! :D
Debug chain:
------------

* [SOLVED] Apparently, I was stupid enough to think that the results would print by themselves without IO.puts. That never happens and if you think so, stop. 
