defmodule Majremind do
  import Majremind.Server

  def main(argv) do
    argv |> parse_args |> process
  end

  defp parse_args(args) do
    parse = OptionParser.parse(args, switches: [help: :boolean, add: :atom, list: :boolean, last: :boolean, open: :string, take_care_of: :string,
    remove: :string], aliases: [h: :help, t: :take_care_of, l: :list, L: :last, r: :remove, a: :add])
    case parse do
      {[], [], []}           -> :help
      {[help: true],  _, _ } -> :help
      {[list: true], [], []} -> :list
      {[last: true], [], []} -> :last
      {[add:    id], [], []} -> {:add, id}
      {[take_care_of: id], [], []} -> {:tco, id}
      {[remove: id], [], []} -> {:remove, id}
    end
  end


  defp process(:help) do
    IO.puts """
    This program helps you to maintain a list of your severs, ordered by date of last update/upgrade of their packets/system.
    It uses DETS and native Elixir features.
    Of course, it can be easily done is ZSH, but I wanted to practice my Ex-fu.

    USAGE: 
    $ ./majremind -h | --help  
    $ ./majremind -l | --list # not an ordered one 
    $ ./majremind -L | --last  
    $ ./majremind -t | --take-care-of "server that has been upgraded recently"
    $ ./majremind -a | --add "new server"
    $ ./majremind -r | --remove "server to remove"

    You can use the functions directly from IEx, by typing:
    $ iex -S mix
    """
    System.halt(0)
  end

  defp process(:list), do: list
  defp process(:last), do: last_server
  defp process({:tco, id}), do: take_care_of(id)
  defp process({:add, id}), do: add_server(id)
  defp process({:remove, id}), do: remove_server(id)

end
