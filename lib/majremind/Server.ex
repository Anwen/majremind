defmodule Majremind.Server do

  @spec last_server :: String.t
  def list do
    {dict, table} = init_db
    keys = Dict.keys(dict)
    for key <- keys do
      IO.puts(key)
    end
    close_db(table)
  end

  @spec last_server :: String.t | {:error, term}
  def last_server do
    {dict, table} = init_db

    Enum.sort(dict, &(elem(&1, 1) > elem(&2, 1))) 
    |> List.last
      |> elem(0) 
        |> Atom.to_string
          |> IO.puts
    close_db(table) 

  end

  @spec take_care_of(String.t) :: :ok | {:error, term}
  def take_care_of(id) do
    {_, table} = init_db
    id = String.to_atom(id)
    new_ts = get_timestamp 
    case :dets.insert(table, {id, new_ts}) do
      :ok -> close_db(table)
      {:error, _} = error -> 
        error
        System.halt(1)
    end
  end

  @spec add_server(String.t) :: :ok | {:error, String.t}
  def add_server(id) do
    {_, table} = init_db
    id = String.to_atom(id)
    case :dets.insert_new(table, {id, 0}) do
      true  -> 
        IO.puts("Server '#{id}' added to database.")
        :ok
        close_db(table)
      false -> 
        {:error, "Server ID already '#{id}' exists in the database!"} 
      {:error, _} = error -> 
        error
        System.halt(1)
    end
  end

  @spec remove_server(String.t) :: :ok | {:error, String.t}
  def remove_server(id) do
    {_, table} = init_db
    id = String.to_atom(id)
    case :dets.delete(table, id) do
      :ok -> 
        {:ok, "Removed Server '#{id}' from database."}
        close_db(table)
        IO.puts("Removed '#{id}' from database.")
      {:error, _} = error -> 
        error
        close_db(table)
        System.halt(1)
    end
  end

  ### Private API ###

  # Shamelessly taken from http://snipplr.com/view/23910/how-to-get-a-timestamp-in-milliseconds-from-erlang/
  defp get_timestamp do
    {mega, sec, micro} = :os.timestamp
    (mega * 1000000 + sec) * 1000000 + micro
  end

  def init_db do
    case :dets.open_file(:"#{System.get_env("HOME")}/.config/majremind/servers", []) do
      {:ok, table} ->
        table
        dict = :dets.traverse(table, fn(x) -> {:continue, x} end)
        {dict, table}
      {:error, _} = error -> 
        error
        System.halt(1)
    end
  end

  def close_db(table) do
    case :dets.close(table) do
      :ok -> :ok
      {:error, _} = error -> 
        error
        System.halt(1)
    end
  end

end
