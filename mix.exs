defmodule Majremind.Mixfile do
  use Mix.Project

  def project do
    [app: :majremind,
     version: "0.0.1",
     elixir: "~> 1.0",
     description: description, 
     escript: escript,
     package: package,
     deps: deps]

  end

  # Configuration for the OTP application
  #
  # Type `mix help compile.app` for more information
  def application do
    [applications: [:logger]]
  end

  def escript do
    [main_module: Majremind]
  end

  defp description do
    """
    A self-maintained database of your updated server which tells you which one needs to be updated.
    It uses Disk Erlang Term Storage for its internal database, located at $HOME/.config/majremind/
    """
  end

  defp package do
    [
      files: ["lib", "mix.exs", "README.md"],
      contributors: ["Anne-Gwenn Kettunen"],
      licenses: ["MIT"],
      links: %{"Bitbucket" => "https://bitbucket.org/Anwen/majremind"}]
  end

  defp deps do
    []
  end
end
